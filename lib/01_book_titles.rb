class Book
  attr_accessor :title
  def initialize

  @capital_exceptions = ["and","in","the","a","an","to","of"]
  end

  def title=(arg)
    arr = arg.split(" ")
    new_arr = []
    arr.each_with_index do |el, idx|
      if idx == 0
        new_arr << el.capitalize
        next
      end
      new_arr << el if @capital_exceptions.include?(el.downcase)

      new_arr << el.capitalize if !@capital_exceptions.include?(el.downcase)
    end
    @title = new_arr.join(" ")
  end


end
