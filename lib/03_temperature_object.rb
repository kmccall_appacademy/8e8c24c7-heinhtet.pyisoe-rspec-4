class Temperature

  def initialize(options={})
    @f = options[:f]
    @c = options[:c]
  end

  def in_fahrenheit
    if @f == nil
        (@c * 9).fdiv(5) + 32
    else
      @f
    end
  end

  def in_celsius
    if @c == nil
        ((@f-32) * 5).fdiv(9)
    else
      @c
    end
  end

  def self.from_fahrenheit(arg)
      return Temperature.new(:f => arg)
  end

  def self.from_celsius(arg)
      return Temperature.new(:c => arg)
  end

end

class Celsius < Temperature
  def initialize(temp)
      @c = temp
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    @f = temp
  end
end
