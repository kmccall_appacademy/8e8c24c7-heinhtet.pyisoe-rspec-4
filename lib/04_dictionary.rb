class Dictionary

  def initialize(options={})
    @dict = options
  end

  def entries
    @dict
  end

  def keywords
    @dict.keys.sort
  end

  def add(entry)
    if entry.class == Hash
      @dict = @dict.merge(entry)
    else
      @dict = @dict.merge({entry => nil})
    end

  end

  def include?(word)
    return true if @dict.keys.include?(word)
    false
  end

  def find(word)
    @dict.select {|k,v| k.include?(word)}
  end

  def printable
    string = []
      @dict.reverse_each do |k,v|
        string << %Q([#{k}] "#{v}") + "\n"
      end
    count = 0
      until count == string.length
      p string[count]
      count += 1
      end
      string
  end
end
