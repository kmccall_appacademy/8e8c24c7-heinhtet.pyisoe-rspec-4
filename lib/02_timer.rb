class Timer
attr_accessor :seconds
def initialize
@seconds = 0
end

def in_format(num)
  fhours = (num / 60) / 60

  fminutes = num / 60
  fminutes -= 60 if fminutes > 60

  fseconds = num
  if fseconds > 60
    fseconds = num % 60
  end

  fhours = "0#{fhours}" if fhours.to_s.length == 1
  fminutes = "0#{fminutes}" if fminutes.to_s.length == 1
  fseconds = "0#{fseconds}" if fseconds.to_s.length == 1

  "#{fhours}:#{fminutes}:#{fseconds}"
end

def time_string
  formatted_seconds = in_format(@seconds)
  "#{formatted_seconds}"
end

end
